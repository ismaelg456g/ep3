# Loja de Instrumentos Musicais

Este projeto implementa uma loja virtual de instrumentos musicais, tendo como objetivo oferecer uma interface para clientes realizarem a compra de instrumentos, e para que os funcionários possam gerenciar os instrumentos, atualizando estoque, criando novos anúncios de instrumentos, editando atributos, e etc.

A interface utilizada foi implementada utilizando bootstrap, as autenticações utilizaram a gem devise.

Há dois casos de uso neste projeto, o dos clientes, e dos funcionários, sendo que para os clientes é permitido comprar e visualizar os produtos, enquanto aos funcionários é permitido também editar, mas não comprar.

As models utilizadas foram as de "produtos", "usuários"(clientes), e "admins"(funcionários).

As controllers utilizadas são:
- "produtos": Para controlar o banco de dados de produtos.
- "paginas": Para controlar algumas páginas padrão.
- "devise", "admins": Geradas pela gem devise para controlar a autenticação.
- "area_funcionarios": Área restrita a funcionários. 

