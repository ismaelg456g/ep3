class AreaFuncionariosController < ApplicationController
	def index
		if user_signed_in?
			redirect_to negado_path
		elsif !admin_signed_in?
			redirect_to sign_in_admin_path
		else
			redirect_to home_path
		end
	end
end
