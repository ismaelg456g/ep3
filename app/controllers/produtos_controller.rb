class ProdutosController < ApplicationController
  before_action :set_produto, only: [:show, :edit, :update, :destroy]

  # GET /produtos
  # GET /produtos.json
  def index
    @produtos = Produto.all
  end

  def search
    @produtos =  Produto.where("nome LIKE ? or descricao LIKE ?", "%" + params[:q] + "%", "%" + params[:q] + "%")
  end

  # GET /produtos/1
  # GET /produtos/1.json
  def show
  end

  # GET /produtos/new

  def new
    if user_signed_in?
      redirect_to negado_path
    elsif !admin_signed_in?
      redirect_to sign_in_admin_path
    else
      @produto = Produto.new
    end
  end

  # GET /produtos/1/edit
  def edit
    if user_signed_in?
      redirect_to negado_path
    elsif !admin_signed_in?
      redirect_to sign_in_admin_path
    end
  end

  # POST /produtos
  # POST /produtos.json
  def create
    @produto = Produto.new(produto_params)

    respond_to do |format|
      if @produto.save
        format.html { redirect_to @produto, notice: 'Produto was successfully created.' }
        format.json { render :show, status: :created, location: @produto }
      else
        format.html { render :new }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /produtos/1
  # PATCH/PUT /produtos/1.json
  def update
    respond_to do |format|
      if @produto.update(produto_params)
        format.html { redirect_to @produto, notice: 'Produto was successfully updated.' }
        format.json { render :show, status: :ok, location: @produto }
      else
        format.html { render :edit }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /produtos/1
  # DELETE /produtos/1.json
  def destroy
    if user_signed_in?
      redirect_to negado_path
    elsif !admin_signed_in?
      redirect_to sign_in_admin_path
    else
      @produto.destroy
      respond_to do |format|
        format.html { redirect_to produtos_url, notice: 'Produto was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  def compra
      if !user_signed_in? and !admin_signed_in?
        redirect_to sign_in_path
      elsif admin_signed_in?
        redirect_to negado_path
      end 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_produto
      @produto = Produto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def produto_params
      params.require(:produto).permit(:nome, :preco, :estoque, :descricao)
    end
end
