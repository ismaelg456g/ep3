Rails.application.routes.draw do
  devise_for :admins, controllers: {
  	sessions: "admins/sessions"
  }
  devise_for :users
  resources :produtos
  get "areaFuncionarios" , to: "area_funcionarios#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root "produtos#index", as: "home"
  get "search", to: "produtos#search" 
  get "contato", to: "paginas#contato"
  get "sobre", to: "paginas#sobre"
  get "negado", to: "paginas#acessoNegado"
  get "compra", to: "produtos#compra"

  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new'
    get 'sign_up', to: 'devise/registrations#new'
    get 'forgot_password', to: 'devise/passwords#new'
    get 'reset_password', to: 'devise/passwords#edit'
  end

  devise_scope :admin do
    get 'sign_in_admin', to: 'devise/sessions#new'
    get 'sign_up_admin', to: 'devise/registrations#new'
    get 'forgot_password_admin', to: 'devise/passwords#new'
    get 'reset_password_admin', to: 'devise/passwords#edit'
  end
end
